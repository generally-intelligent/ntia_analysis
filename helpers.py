import collections
import math
from typing import Mapping
from typing import Sequence
from typing import Set
from typing import Tuple

from constants import ANSWER_OVERRIDES_BY_STUDY_NAME
from constants import NO_CHOICE
from constants import UNSURE_CHOICE
from constants import YES_CHOICE
from constants import YES_NO_POLICY_QUESTIONS
from custom_types import DocumentID
from custom_types import QuestionType
from data_model import ProlificParticipantAnswer
from studies import PolicyQAStudy
from studies import build_study_from_prolific_participant_answers
from studies import get_agreement_by_question_between_studies
from studies import get_all_agreements_by_question_with_particular_answer
from studies import get_differences_by_document_id
from studies import get_mode_answer_by_question_and_document_id
from utils import first


import attr
from tabulate import tabulate

def create_studies_from_prolific_submission_answers(
    all_prolific_answers: Sequence[ProlificParticipantAnswer],
) -> Tuple[PolicyQAStudy, PolicyQAStudy, PolicyQAStudy, PolicyQAStudy]:
    all_prolific_answers = [
        a
        for a in all_prolific_answers
        if str(a.submission_answer.answer) in (YES_CHOICE, NO_CHOICE, UNSURE_CHOICE)
        and a.submission_answer.question in set(question for question in YES_NO_POLICY_QUESTIONS)
    ]
    imbue_prolific_answers = [a for a in all_prolific_answers if "imbue" in a.prolific_participant_id]
    imbue_gold_prolific_study = build_study_from_prolific_participant_answers(
        name="gold_imbue",
        all_prolific_participant_answers=imbue_prolific_answers,
        answer_overrides=ANSWER_OVERRIDES_BY_STUDY_NAME["gold_imbue"],
    )

    gold_document_ids = set(imbue_gold_prolific_study.document_ids)

    non_gold_prolific_answers = [
        a
        for a in all_prolific_answers
        if "imbue" not in a.prolific_participant_id and a.submission_answer.document_id not in gold_document_ids
    ]
    non_gold_prolific_study = build_study_from_prolific_participant_answers(
        name="non_gold_prolific", all_prolific_participant_answers=non_gold_prolific_answers
    )

    gold_prolific_answers = [
        a
        for a in all_prolific_answers
        if "imbue" not in a.prolific_participant_id and a.submission_answer.document_id in gold_document_ids
    ]
    gold_prolific_study = build_study_from_prolific_participant_answers(
        name="gold_prolific", all_prolific_participant_answers=gold_prolific_answers
    )

    non_imbue_prolific_study = build_study_from_prolific_participant_answers(
        name="non_imbue_prolific",
        all_prolific_participant_answers=non_gold_prolific_answers + gold_prolific_answers,
    )

    return imbue_gold_prolific_study, gold_prolific_study, non_gold_prolific_study, non_imbue_prolific_study


def _get_mode(answers: Sequence[str]) -> str:
    most_common = collections.Counter(answers).most_common()
    mode_answer, count = first(most_common)
    return mode_answer


def display_comparison_to_gold_set(
    non_imbue_studies: Tuple[PolicyQAStudy, ...], imbue_gold_prolific_study: PolicyQAStudy
) -> None:
    # answer_counts_by_question = collections.defaultdict(lambda: collections.defaultdict(int))
    #
    # answers_by_question = collections.defaultdict(list)
    # for answer in get_submission_answers(imbue_gold_prolific_study):
    #     answers_by_question[answer.question].append(answer.answer)
    #
    # for question, answers in answers_by_question.items():
    #     answer = _get_mode(answers)
    #     answer_counts_by_question[question][answer] += 1

    agreements_by_study = {}
    differences_by_study = {}
    yes_agreements_by_study = {}
    no_agreements_by_study = {}
    for study in non_imbue_studies:
        differences_by_document_id = get_differences_by_document_id(study_a=imbue_gold_prolific_study, study_b=study)
        differences_by_question = collections.defaultdict(int)

        yes_agreements_by_study[study.id] = get_all_agreements_by_question_with_particular_answer(
            candidate_study=study, truthy_study=imbue_gold_prolific_study, answer="Yes"
        )
        no_agreements_by_study[study.id] = get_all_agreements_by_question_with_particular_answer(
            candidate_study=study, truthy_study=imbue_gold_prolific_study, answer="No"
        )

        for document_id, answer_pair_by_question in differences_by_document_id.items():
            for question, answer_pair in answer_pair_by_question.items():
                differences_by_question[question] += 1

        differences_by_study[study.id] = differences_by_question

        agreements_by_study[study.id] = get_agreement_by_question_between_studies(
            study_a=imbue_gold_prolific_study,
            study_b=study,
            is_using_mode=True,
        )

    agreements_by_study_id = collections.defaultdict(list)
    all_agreements_by_question = collections.defaultdict(list)
    total_false_positives_by_study_id = collections.defaultdict(int)
    total_false_negatives_by_study_id = collections.defaultdict(int)
    total_yes_count_by_study_id = collections.defaultdict(int)
    total_no_count_by_study_id = collections.defaultdict(int)

    for study_id, agreement_by_question in agreements_by_study.items():
        for question, agreement in agreement_by_question.items():
            yes_gold_count = yes_agreements_by_study[study_id].get(question, [])
            no_gold_count = no_agreements_by_study[study_id].get(question, [])

            total_yes_count_by_study_id[study_id] += len(yes_gold_count)
            total_no_count_by_study_id[study_id] += len(no_gold_count)

            false_negative = len(yes_gold_count) - sum(yes_gold_count)
            false_positive = len(no_gold_count) - sum(no_gold_count)

            true_negative = sum(no_gold_count)
            true_positive = sum(yes_gold_count)

            # print("confusion matrix for", question, "in", study_id)
            # print(true_positive, false_negative)
            # print(false_positive, true_negative)
            # print()

            # false positive = gold is yes, but we said no
            false_positive_rate = false_positive / (false_positive + true_negative)
            # false negative = gold is no, but we said yes
            false_negative_rate = false_negative / (false_negative + true_positive)

            all_agreements_by_question[question].append(f"{agreement * 100:.1f}%")
            all_agreements_by_question[question].append(
                f"{false_positive_rate*100:.1f}%" if false_positive_rate != 0.0 else ""
            )  # _rate
            all_agreements_by_question[question].append(
                f"{false_negative_rate*100:.1f}%" if false_negative_rate != 0.0 else ""
            )  # _rate * 100:.1f
            agreements_by_study_id[study_id].append(agreement)

            total_false_negatives_by_study_id[study_id] += false_negative
            total_false_positives_by_study_id[study_id] += false_positive

    table_rows = []
    for question, agreements in all_agreements_by_question.items():
        table_rows.append([f'"{question}"'] + agreements)

    table_row = ["Total"]
    for study in non_imbue_studies:
        table_row.append(f"{sum(agreements_by_study_id[study.id]) * 100 / len(agreements_by_study_id[study.id]):.2f}%")
        fpr = total_false_positives_by_study_id[study.id] / (
            total_false_positives_by_study_id[study.id] + total_no_count_by_study_id[study.id]
        )
        table_row.append(f"{fpr * 100:.1f}%")
        fnr = total_false_negatives_by_study_id[study.id] / (
            total_false_negatives_by_study_id[study.id] + total_yes_count_by_study_id[study.id]
        )
        table_row.append(f"{fnr * 100:.1f}%")

    table_rows.append(table_row)
    headers = [
        "Questions",
        "Prolific",
        "Prolific FPR",
        "Prolific FNR",
        "LLM",
        "Prolific FPR",
        "Prolific FNR",
    ]
    print(
        tabulate(
            table_rows,
            headers=headers,
            maxcolwidths=[None, None, None, None, None, None],
        )
    )
    print()


def _get_variance(data):
    n = len(data)
    mean = sum(data) / n
    var = sum((x - mean) ** 2 for x in data) / n
    return var


def _get_standard_error(data):
    return math.sqrt(_get_variance(data)) / math.sqrt(len(data))


@attr.s(auto_attribs=True, frozen=True, kw_only=True)
class StudyResults:
    answered_yes: int
    answered_no: int


def get_answer_counts_by_question(
    study: PolicyQAStudy, document_ids: Set[DocumentID]
) -> Mapping[QuestionType, StudyResults]:
    yes_count = collections.Counter()
    no_count = collections.Counter()
    for (question, document_id), answers in study.answers_by_question_and_document_id.items():
        if document_id not in document_ids:
            continue
        answer = get_mode_answer_by_question_and_document_id(study=study, question=question, document_id=document_id)
        yes_count[question] += answer == YES_CHOICE
        no_count[question] += answer != YES_CHOICE

    return {
        question: StudyResults(answered_yes=yes_count[question], answered_no=no_count[question])
        for question in yes_count.keys()
    }


def display_results_for_study_and_given_document_ids(
    study: PolicyQAStudy, document_ids: Set[DocumentID], result_name: str
) -> None:
    num_submissions = len(document_ids)

    headers = ["Question", "Total Submissions", "Total Yes", "Total No", "Percent Yes", "Percent No", "Error"]
    table_rows = []
    for question, result in get_answer_counts_by_question(study=study, document_ids=document_ids).items():
        standard_error = _get_standard_error(
            [1 for _ in range(result.answered_yes)] + [0 for _ in range(result.answered_no)]
        )

        table_rows.append(
            [
                f'"{question}"',
                str(num_submissions),
                str(result.answered_yes),
                str(result.answered_no),
                str(f"{result.answered_yes / num_submissions * 100:.2f}%"),
                str(f"{result.answered_no / num_submissions * 100:.2f}%"),
                str(f"{standard_error * 100:.2f}%"),
            ]
        )

    print(f"== {result_name.upper()} ==")
    print(tabulate(table_rows, headers=headers, maxcolwidths=[50, None, None, None, None, None, None]))


def display_answer_breakdowns_for_study(study: PolicyQAStudy) -> None:
    all_submission_ids = set()
    submission_ids_with_personal_harm = set()
    submission_ids_who_are_artists = set()

    count_unsure = 0
    count_non_uniform = 0
    for (question, document_id), answers in study.answers_by_question_and_document_id.items():
        answer_strings = [a.answer for a in answers]
        if len(set(answer_strings)) != 1:
            count_non_uniform += 1
        if "Unsure" in answer_strings:
            count_unsure += 1
        all_submission_ids.add(document_id)
        answer = get_mode_answer_by_question_and_document_id(study=study, question=question, document_id=document_id)
        if question == "Does the author describe a personal harm they have experienced from AI?":
            if answer == YES_CHOICE:
                submission_ids_with_personal_harm.add(document_id)
        if question == "Does the author state they are an artist, writer, or work in a creative field?":
            if answer == YES_CHOICE:
                submission_ids_who_are_artists.add(document_id)

    display_results_for_study_and_given_document_ids(study=study, document_ids=all_submission_ids, result_name="All")
    print()
    display_results_for_study_and_given_document_ids(
        study=study, document_ids=submission_ids_with_personal_harm, result_name="Personal Harm"
    )
    print()
    display_results_for_study_and_given_document_ids(
        study=study, document_ids=submission_ids_who_are_artists, result_name="Artists"
    )
    print()
