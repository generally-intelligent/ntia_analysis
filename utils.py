from typing import Iterable
from typing import Optional
from typing import TypeVar

T = TypeVar("T")


def first(iterable: Iterable[T]) -> Optional[T]:
    return next(iter(iterable), None)
