# %%
import json
from pathlib import Path

from data_model import ProlificParticipantAnswer
from data_model import SubmissionAnswer
from helpers import create_studies_from_prolific_submission_answers
from helpers import display_answer_breakdowns_for_study
from helpers import display_comparison_to_gold_set
from studies import build_study_for_llm_results
from studies import get_study_comparisons

# %%

all_prolific_answers_path = Path("data/all_prolific_answers.jsonl")
all_prolific_answers = tuple(
    ProlificParticipantAnswer.from_dict(json.loads(data))
    for data in all_prolific_answers_path.read_text().splitlines()
)


# %%

llm_answers_path = Path("data/llm_answers.jsonl")
llm_answers = tuple(SubmissionAnswer.from_dict(json.loads(data)) for data in llm_answers_path.read_text().splitlines())
llm_study = build_study_for_llm_results(
    llm_submission_answers=llm_answers, study_id="38551551-a43e-4b25-b75b-e5ac2136aaf8", name="llm"
)

# %%

(
    imbue_gold_prolific_study,
    gold_prolific_study,
    non_gold_prolific_study,
    non_imbue_prolific_study,
) = create_studies_from_prolific_submission_answers(all_prolific_answers=all_prolific_answers)

# %%

non_imbue_studies = (gold_prolific_study, llm_study)

print("HUMANS AND LLM vs GOLD:")
display_comparison_to_gold_set(
    non_imbue_studies=non_imbue_studies, imbue_gold_prolific_study=imbue_gold_prolific_study
)

# %%

print("LLM:")
display_answer_breakdowns_for_study(study=llm_study)

# %%

print("HUMANS:")
display_answer_breakdowns_for_study(study=non_imbue_prolific_study)

# %%

# A more fine-grained break down of the comparison between the imbue gold set and the non-imbue studies
comparisons = get_study_comparisons((imbue_gold_prolific_study, *non_imbue_studies))

for comparison in comparisons:
    pass
    # comparison.print_summary()  # NOTE: uncomment to visualize the comparison
