import collections
import hashlib
import itertools
from typing import Any
from typing import Dict
from typing import List
from typing import Mapping
from typing import Optional
from typing import Sequence
from typing import Set
from typing import Tuple

import attr
from constants import NO_CHOICE
from constants import UNSURE_CHOICE
from constants import YES_CHOICE
from custom_types import DocumentID
from custom_types import QuestionAndDocumentIDType
from custom_types import QuestionType
from data_model import IndividualSubmission
from data_model import ProlificParticipantAnswer
from data_model import SubmissionAnswer
from tabulate import tabulate
from utils import first


@attr.s(auto_attribs=True, frozen=True, kw_only=True)
class PolicyQAStudy:
    id: str
    name: str
    is_from_llm: bool
    answers_by_question_and_document_id: Mapping[Tuple[QuestionType, DocumentID], Tuple[SubmissionAnswer, ...]]
    meta: Optional[Mapping[str, Any]] = None
    description: Optional[str] = None

    @property
    def short_name(self) -> str:
        short_name = self.name.split("__")[0]
        return f"{short_name} ({self.id})"

    @property
    def document_ids(self) -> Set[DocumentID]:
        return set(list(zip(*self.answers_by_question_and_document_id.keys()))[1])

    @property
    def questions(self) -> Set[QuestionType]:
        return set(list(zip(*self.answers_by_question_and_document_id.keys()))[0])


def get_mode_answer_by_question_and_document_id(
    study: PolicyQAStudy, question: QuestionType, document_id: DocumentID
) -> str:
    answers = study.answers_by_question_and_document_id[(question, document_id)]
    answer_strings = [answer.answer for answer in answers]
    most_common = collections.Counter(answer_strings).most_common()
    mode_answer, count = first(most_common)
    return mode_answer


def get_submission_answers(
    study: PolicyQAStudy, included_document_ids: Optional[Set[str]] = None
) -> Tuple[SubmissionAnswer, ...]:
    all_answers = tuple(itertools.chain.from_iterable(study.answers_by_question_and_document_id.values()))
    if included_document_ids is None:
        return all_answers
    else:
        return tuple(answer for answer in all_answers if answer.document_id in included_document_ids)


def get_questions_with_unsure_answer(study: PolicyQAStudy) -> Tuple[Tuple[QuestionType, DocumentID], ...]:
    question_and_document_ids = []
    for (question, document_id), submission_answers in study.answers_by_question_and_document_id.items():
        if any(answer.answer == UNSURE_CHOICE for answer in submission_answers):
            question_and_document_ids.append((question, document_id))
    return tuple(question_and_document_ids)


def _update_unsure_answer_to_no(answer: SubmissionAnswer) -> SubmissionAnswer:
    return SubmissionAnswer(
        document_id=answer.document_id,
        study_id=answer.study_id,
        sample_id=answer.sample_id,
        source=answer.source,
        question=answer.question,
        answer=NO_CHOICE if answer.answer == UNSURE_CHOICE else answer.answer,
    )


def update_unsure_answers_to_no(
    submission_answers: Sequence[SubmissionAnswer],
) -> Tuple[SubmissionAnswer, ...]:
    return tuple(
        _update_unsure_answer_to_no(answer) if answer.answer == UNSURE_CHOICE else answer
        for answer in submission_answers
    )


def build_study_from_prolific_participant_answers(
    name: str,
    all_prolific_participant_answers: Sequence[ProlificParticipantAnswer],
    is_making_unsure_no: bool = True,
    answer_overrides: Optional[Mapping[Tuple[QuestionType, DocumentID], str]] = None,
) -> PolicyQAStudy:
    study_id = generate_id_from_string(",".join([a.prolific_participant_id for a in all_prolific_participant_answers]))
    answers_by_question_and_document_id: Dict[
        Tuple[QuestionType, DocumentID], Tuple[SubmissionAnswer, ...]
    ] = collections.defaultdict(tuple)
    for prolific_participant_answer in all_prolific_participant_answers:
        key = (
            QuestionType(prolific_participant_answer.submission_answer.question),
            DocumentID(prolific_participant_answer.submission_answer.document_id),
        )
        if answer_overrides is not None and key in answer_overrides:
            question, document_id = key
            answer = SubmissionAnswer(
                document_id=document_id,
                study_id=study_id,
                sample_id="manual_gold",
                source="gold",
                question=question,
                answer=answer_overrides[key],
            )
            if is_making_unsure_no:
                answer = _update_unsure_answer_to_no(answer)
            answers_by_question_and_document_id[key] += (answer,)
        else:
            answers_by_question_and_document_id[key] += (prolific_participant_answer.submission_answer,)

    return PolicyQAStudy(
        id=study_id,
        name=name,
        is_from_llm=False,
        answers_by_question_and_document_id=answers_by_question_and_document_id,
    )


def build_study_for_llm_results(
    llm_submission_answers: Tuple[SubmissionAnswer, ...],
    study_id: str,
    name: str,
    meta: Optional[Mapping[str, Any]] = None,
    is_making_unsure_no: bool = True,
) -> PolicyQAStudy:
    answers_by_question_and_document_id: Dict[
        Tuple[QuestionType, DocumentID], Tuple[SubmissionAnswer, ...]
    ] = collections.defaultdict(tuple)
    for answer in llm_submission_answers:
        answers_by_question_and_document_id[(QuestionType(answer.question), DocumentID(answer.document_id))] += (
            _update_unsure_answer_to_no(answer) if is_making_unsure_no else answer,
        )
    return PolicyQAStudy(
        id=study_id,
        name=name,
        is_from_llm=True,
        meta=meta,
        answers_by_question_and_document_id=answers_by_question_and_document_id,
    )


def get_shared_questions_and_document_ids_between_studies(
    study_a: PolicyQAStudy, study_b: PolicyQAStudy
) -> Tuple[Tuple[QuestionType, DocumentID], ...]:
    shared_questions: Set[QuestionType] = study_a.questions & study_b.questions
    if len(shared_questions) == 0:
        print("No shared questions or document ids between studies")
        return tuple()
    shared_document_ids: Set[DocumentID] = study_a.document_ids & study_b.document_ids
    if len(shared_document_ids) == 0:
        print("No shared document ids between studies")
        return tuple()

    keys = []
    for document_id in shared_document_ids:
        for question in shared_questions:
            keys.append((question, document_id))

    return tuple(keys)


def get_questions_and_document_ids_with_differing_answers_between_studies(
    study_a: PolicyQAStudy,
    study_b: PolicyQAStudy,
    shared_keys: Optional[Tuple[QuestionAndDocumentIDType, ...]] = None,
    ignored_question_and_document_ids: Optional[Set[QuestionAndDocumentIDType]] = None,
) -> Tuple[Tuple[QuestionType, DocumentID], ...]:
    if shared_keys is None:
        shared_keys: Tuple[QuestionAndDocumentIDType, ...] = get_shared_questions_and_document_ids_between_studies(
            study_a=study_a, study_b=study_b
        )
    result = []
    for question, document_id in shared_keys:
        if (
            ignored_question_and_document_ids is not None
            and (question, document_id) in ignored_question_and_document_ids
        ):
            continue
        if get_mode_answer_by_question_and_document_id(
            study=study_a, question=question, document_id=document_id
        ) != get_mode_answer_by_question_and_document_id(study=study_b, question=question, document_id=document_id):
            result.append((question, document_id))
    return tuple(result)


def _print_list_in_rows(list_to_print: Sequence[str], num_columns: int = 5) -> None:
    for i in range(0, len(list_to_print), num_columns):
        print("\t".join(list_to_print[i : i + num_columns]))


@attr.s(auto_attribs=True, frozen=True, kw_only=True)
class StudyComparison:
    study_a: PolicyQAStudy
    study_b: PolicyQAStudy

    def get_mean_agreement(
        self,
        is_using_mode: bool = True,
        ignored_question_and_document_ids: Optional[Set[QuestionAndDocumentIDType]] = None,
    ) -> float:
        agreement_by_question = get_agreement_by_question_between_studies(
            study_a=self.study_a,
            study_b=self.study_b,
            ignored_question_and_document_ids=ignored_question_and_document_ids,
            is_using_mode=is_using_mode,
        )
        return sum(agreement_by_question.values()) / len(agreement_by_question)

    def print_summary(
        self,
        is_using_mode: bool = True,
        included_document_ids: Optional[Set[str]] = None,
        ignored_question_and_document_ids: Optional[Set[QuestionAndDocumentIDType]] = None,
    ) -> None:
        shared_questions_and_document_ids = get_shared_questions_and_document_ids_between_studies(
            study_a=self.study_a, study_b=self.study_b
        )

        if len(shared_questions_and_document_ids) == 0:
            print("No shared questions or document ids between studies")
            return

        questions, document_ids = zip(*shared_questions_and_document_ids)

        if included_document_ids is not None:
            document_ids = set(included_document_ids)

        agreement_by_question = get_agreement_by_question_between_studies(
            study_a=self.study_a,
            study_b=self.study_b,
            included_document_ids=document_ids,
            ignored_question_and_document_ids=ignored_question_and_document_ids,
            is_using_mode=is_using_mode,
        )

        questions_and_document_ids_with_differing_answers = (
            get_questions_and_document_ids_with_differing_answers_between_studies(
                study_a=self.study_a,
                study_b=self.study_b,
                shared_keys=shared_questions_and_document_ids,
                ignored_question_and_document_ids=ignored_question_and_document_ids,
            )
        )

        print("== OVERVIEW ==")
        print(f"Study A: {self.study_a.name}")
        print(f"Study B: {self.study_b.name}")
        # print(f"Unique document ids ({len(set(document_ids))})")
        # if len(document_ids) <= 100:
        #     _print_list_in_rows(sorted(set(document_ids)))
        #     print()
        # print(f"Unique questions ({len(set(questions))}):")
        # print("\n".join(set(questions)))
        # print()
        print(f"Total number of answers shared between studies: {len(shared_questions_and_document_ids)}")
        print(
            f"Total number of answers differing between studies: {len(questions_and_document_ids_with_differing_answers)}"
        )
        print()
        print("== AGREEMENT ACROSS ALL SUBMISSIONS ==")
        mean_agreement = self.get_mean_agreement(ignored_question_and_document_ids=ignored_question_and_document_ids)
        print(f"Mean agreement: {mean_agreement:.3f}")
        print("Agreement by question:")
        for question, agreement in agreement_by_question.items():
            print(f"{question}: {agreement:.3f}")
        print()

        print("== AGREEMENT BY ANSWER CHOICE ==")
        for answer_choice in (YES_CHOICE, NO_CHOICE):
            all_agreements_by_question = get_all_agreements_by_question_with_particular_answer(
                candidate_study=self.study_b,
                truthy_study=self.study_a,
                answer=answer_choice,
                included_document_ids=document_ids,
                ignored_question_and_document_ids=ignored_question_and_document_ids,
            )
            answer_agreement_by_question = {k: sum(v) / len(v) for k, v in all_agreements_by_question.items()}
            total_agreement_count = sum(list(itertools.chain.from_iterable(all_agreements_by_question.values())))
            total_answer_count = len(list(itertools.chain.from_iterable(all_agreements_by_question.values())))
            mean_agreement = sum(answer_agreement_by_question.values()) / len(answer_agreement_by_question)
            if mean_agreement == 1.0:
                continue
            print(f"Agreed on {total_agreement_count}/{total_answer_count} answers for {answer_choice}")
            print(f"Mean agreement for {answer_choice}: {mean_agreement:.3f}")
            print(f"Agreement by question with answer:")
            for question, agreement in all_agreements_by_question.items():
                mean_agreement_per_question = sum(agreement) / len(agreement)
                if mean_agreement_per_question != 1.0:
                    print(f"{question}: {sum(agreement)}/{len(agreement)}")
            print()

    def print_differences(
        self,
        submissions_by_id: Mapping[str, IndividualSubmission],
        included_document_ids: Optional[Set[str]] = None,
        ignored_question_and_document_ids: Optional[Set[QuestionAndDocumentIDType]] = None,
    ) -> None:
        for document_id, diff in get_differences_by_document_id(
            study_a=self.study_a,
            study_b=self.study_b,
            ignored_question_and_document_ids=ignored_question_and_document_ids,
        ).items():
            if included_document_ids is not None and document_id not in included_document_ids:
                continue
            print(document_id)
            print("DOCUMENT")
            print("-" * 80)
            submission = submissions_by_id[document_id]
            print(submission.document)
            print("-" * 80)
            print()
            for question, (answer_a, answer_b) in diff.items():
                print("== QUESTION ==")
                print(f"{question}\n")
                print(tabulate([[self.study_a.short_name, answer_a], [self.study_b.short_name, answer_b]]))
                print()
            print()


def get_differences_by_document_id(
    study_a: PolicyQAStudy,
    study_b: PolicyQAStudy,
    ignored_question_and_document_ids: Optional[Set[QuestionAndDocumentIDType]] = None,
) -> Mapping[DocumentID, Mapping[QuestionType, Tuple[str, str]]]:
    differences_by_document_id: Dict[DocumentID, Dict[QuestionType, Tuple[str, str]]] = collections.defaultdict(dict)
    shared_keys = get_shared_questions_and_document_ids_between_studies(study_a=study_a, study_b=study_b)
    differing_answers: Tuple[
        Tuple[QuestionType, DocumentID], ...
    ] = get_questions_and_document_ids_with_differing_answers_between_studies(
        study_a=study_a, study_b=study_b, shared_keys=shared_keys
    )
    for question, document_id in differing_answers:
        if (
            ignored_question_and_document_ids is not None
            and (question, document_id) in ignored_question_and_document_ids
        ):
            continue
        differences_by_document_id[document_id][question] = (
            get_mode_answer_by_question_and_document_id(study=study_a, question=question, document_id=document_id),
            get_mode_answer_by_question_and_document_id(study=study_b, question=question, document_id=document_id),
        )
    return differences_by_document_id


def get_agreement_by_question_between_studies(
    study_a: PolicyQAStudy,
    study_b: PolicyQAStudy,
    is_using_mode: bool,
    included_document_ids: Optional[Set[str]] = None,
    ignored_question_and_document_ids: Optional[Set[QuestionAndDocumentIDType]] = None,
) -> Mapping[str, float]:
    shared_keys = get_shared_questions_and_document_ids_between_studies(study_a=study_a, study_b=study_b)
    agreement_by_question_by_document_id = collections.defaultdict(dict)
    for question, document_id in shared_keys:
        if (
            ignored_question_and_document_ids is not None
            and (question, document_id) in ignored_question_and_document_ids
        ):
            continue
        if included_document_ids is not None and document_id not in included_document_ids:
            continue
        if is_using_mode:
            answer_a = get_mode_answer_by_question_and_document_id(
                study=study_a, question=question, document_id=document_id
            )
            answer_b = get_mode_answer_by_question_and_document_id(
                study=study_b, question=question, document_id=document_id
            )
        else:
            raise NotImplementedError("Only mode is implemented")
        agreement_by_question_by_document_id[document_id][question] = answer_a == answer_b
    return get_agreement_by_question(agreement_by_question_by_document_id)


def get_intra_agreement(study: PolicyQAStudy) -> Mapping[Tuple[QuestionType, DocumentID], Mapping[str, int]]:
    intra_agreement: Dict[Tuple[QuestionType, DocumentID], Dict[str, int]] = collections.defaultdict(
        lambda: collections.defaultdict(int)
    )
    for answers in study.answers_by_question_and_document_id.values():
        for answer in answers:
            intra_agreement[(QuestionType(answer.question), DocumentID(answer.document_id))][answer.answer] += 1
    return intra_agreement


def get_all_agreements_by_question_with_particular_answer(
    candidate_study: PolicyQAStudy,
    truthy_study: PolicyQAStudy,
    answer: str,
    included_document_ids: Optional[Set[str]] = None,
    ignored_question_and_document_ids: Optional[Set[Tuple[str, str]]] = None,
) -> Mapping[QuestionType, List[bool]]:
    all_agreement_by_question = collections.defaultdict(list)
    shared_questions_and_document_ids = get_shared_questions_and_document_ids_between_studies(
        study_a=candidate_study, study_b=truthy_study
    )
    for question, document_id in shared_questions_and_document_ids:
        if included_document_ids is not None and document_id not in included_document_ids:
            continue
        if (
            ignored_question_and_document_ids is not None
            and (question, document_id) in ignored_question_and_document_ids
        ):
            continue
        truthy_answer = get_mode_answer_by_question_and_document_id(
            study=truthy_study, question=question, document_id=document_id
        )
        if truthy_answer == answer:
            candidate_answer = get_mode_answer_by_question_and_document_id(
                study=candidate_study, question=question, document_id=document_id
            )
            all_agreement_by_question[question].append(candidate_answer == answer)
    return {k: tuple(v) for k, v in all_agreement_by_question.items()}


def get_study_comparisons(studies: Tuple[PolicyQAStudy, ...]) -> Tuple[StudyComparison, ...]:
    study_comparisons: List[StudyComparison] = []
    for study_a, study_b in itertools.combinations(studies, r=2):
        study_comparisons.append(StudyComparison(study_a=study_a, study_b=study_b))
    return tuple(study_comparisons)


def get_agreement_by_question(
    agreement_by_question_by_submission: Mapping[str, Mapping[str, bool]]
) -> Mapping[str, float]:
    all_agreements_by_question = collections.defaultdict(list)
    for agreement_by_question in agreement_by_question_by_submission.values():
        for question, agreement in agreement_by_question.items():
            all_agreements_by_question[question].append(agreement)
    return {question: sum(agreements) / len(agreements) for question, agreements in all_agreements_by_question.items()}


def generate_id_from_string(s: str) -> str:
    return hashlib.md5(s.encode()).hexdigest()
