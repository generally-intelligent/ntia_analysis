from typing import NewType
from typing import Tuple

QuestionType = NewType("QuestionType", str)
DocumentID = NewType("DocumentID", str)
ComparisonID = NewType("ComparisonID", Tuple[str, str])
QuestionAndDocumentIDType = Tuple[QuestionType, DocumentID]
