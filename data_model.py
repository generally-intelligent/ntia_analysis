from typing import Any
from typing import Dict
from typing import Literal
from typing import Mapping
from typing import Optional
from typing import Tuple

import attr
from constants import ATTACHMENT_CLASSIFICATIONS_BY_SUBMISSION_ID


@attr.s(auto_attribs=True, frozen=True, kw_only=True)
class SubmissionAnswer:
    document_id: str
    study_id: str
    sample_id: str
    source: Literal["llm", "human", "gold"]
    question: str
    answer: str

    def to_dict(self) -> Dict[str, Any]:
        return {
            "document_id": self.document_id,
            "study_id": self.study_id,
            "sample_id": self.sample_id,
            "source": self.source,
            "question": self.question,
            "answer": self.answer,
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> "SubmissionAnswer":
        return cls(
            document_id=d["document_id"],
            study_id=d["study_id"],
            sample_id=d["sample_id"],
            source=d["source"],
            question=d["question"],
            answer=d["answer"],
        )


@attr.s(auto_attribs=True, frozen=True, kw_only=True)
class ProlificParticipantAnswer:
    prolific_participant_id: str
    prolific_study_id: str
    session_id: str
    form_id: str
    submission_answer: SubmissionAnswer

    def to_dict(self) -> Dict[str, Any]:
        return {
            "prolific_participant_id": self.prolific_participant_id,
            "prolific_study_id": self.prolific_study_id,
            "session_id": self.session_id,
            "form_id": self.form_id,
            "submission_answer": self.submission_answer.to_dict(),
        }

    @classmethod
    def from_dict(cls, d: Dict[str, Any]) -> "ProlificParticipantAnswer":
        return cls(
            prolific_participant_id=d["prolific_participant_id"],
            prolific_study_id=d["prolific_study_id"],
            session_id=d["session_id"],
            form_id=d["form_id"],
            submission_answer=SubmissionAnswer.from_dict(d["submission_answer"]),
        )


@attr.s(auto_attribs=True, frozen=True)
class ParsedAttachment:
    name: str
    url: str
    extension: str
    text: str


@attr.s(auto_attribs=True, frozen=True)
class IndividualSubmission:
    document_id: str
    first_name: str
    last_name: str
    city: str
    state_or_province: str
    zip_or_postal_code: str
    country: str
    organization_name: str
    type: str
    submitter_representative: str
    comment: str
    parsed_attachments: Tuple[ParsedAttachment, ...]

    def has_attachments(self) -> bool:
        return len(self.parsed_attachments) > 0

    def has_valid_attachments(self) -> bool:
        return len(self.parsed_attachments) > 0 and all(
            x.extension in {"pdf", "docx", "txt"} for x in self.parsed_attachments
        )

    @property
    def attachment_classifications(self) -> Mapping[str, str]:
        return ATTACHMENT_CLASSIFICATIONS_BY_SUBMISSION_ID.get(self.document_id)

    def has_accepted_attachments(self) -> bool:
        for parsed_attachment in self.parsed_attachments:
            if self.attachment_classifications.get(parsed_attachment.name) == "accept":
                return True
        return False

    def _attempt_to_get_attachment_text(self) -> Optional[str]:
        if self.document_id in ATTACHMENT_CLASSIFICATIONS_BY_SUBMISSION_ID:
            accepted_attachment_texts = []
            for parsed_attachment in self.parsed_attachments:
                if self.attachment_classifications[parsed_attachment.name] == "accept":
                    accepted_attachment_texts.append(parsed_attachment.text)

            if len(accepted_attachment_texts) > 1:
                # NOTE: @bryden did a check and after @vincent's analysis, there are no submissions with more than one
                # accepted attachment
                raise ValueError(f"More than one accepted attachment text for document_id={self.document_id}")
            elif len(accepted_attachment_texts) == 1:
                return accepted_attachment_texts[0]
            else:
                return None
        else:
            return None

    @property
    def document(self) -> str:
        if self.has_attachments():
            attachment_text = self._attempt_to_get_attachment_text()
            if attachment_text is not None:
                return self.comment + "\n\nINCLUDED ATTACHMENT:\n\n" + attachment_text
        return self.comment
